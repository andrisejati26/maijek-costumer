package com.mai.timor.api.service;

import com.mai.timor.model.json.book.CheckStatusTransaksiRequest;
import com.mai.timor.model.json.book.CheckStatusTransaksiResponse;
import com.mai.timor.model.json.book.GetAdditionalMboxResponseJson;
import com.mai.timor.model.json.book.GetBarangTokoRequestJson;
import com.mai.timor.model.json.book.GetBarangTokoResponseJson;
import com.mai.timor.model.json.book.GetDataLaundryRequestJson;
import com.mai.timor.model.json.book.GetDataLaundryResponseJson;
import com.mai.timor.model.json.book.GetDataMserviceResponseJson;
import com.mai.timor.model.json.book.GetDataRestoByKategoriRequestJson;
import com.mai.timor.model.json.book.GetDataRestoByKategoriResponseJson;
import com.mai.timor.model.json.book.GetDataRestoRequestJson;
import com.mai.timor.model.json.book.GetDataRestoResponseJson;
import com.mai.timor.model.json.book.GetDataRestoranRequestJson;
import com.mai.timor.model.json.book.GetDataRestoranResponseJson;
import com.mai.timor.model.json.book.GetDataTokoByKategoriRequestJson;
import com.mai.timor.model.json.book.GetDataTokoByKategoriResponseJson;
import com.mai.timor.model.json.book.GetDataTokoRequestJson;
import com.mai.timor.model.json.book.GetDataTokoResponseJson;
import com.mai.timor.model.json.book.GetLayananLaundryRequestJson;
import com.mai.timor.model.json.book.GetLayananLaundryResponseJson;
import com.mai.timor.model.json.book.LiatLokasiDriverResponse;
import com.mai.timor.model.json.book.RequestBarangRequestJson;
import com.mai.timor.model.json.book.RequestBarangResponseJson;
import com.mai.timor.model.json.book.RequestLaundryRequestJson;
import com.mai.timor.model.json.book.RequestLaundryResponseJson;
import com.mai.timor.model.json.book.SearchElectronicRequest;
import com.mai.timor.model.json.book.SearchElectronicResponse;
import com.mai.timor.model.json.book.SearchLaundryRequest;
import com.mai.timor.model.json.book.SearchLaundryResponse;
import com.mai.timor.model.json.book.detailTransaksi.GetDataTransaksiMElectronicResponse;
import com.mai.timor.model.json.book.detailTransaksi.GetDataTransaksiMFoodResponse;
import com.mai.timor.model.json.book.detailTransaksi.GetDataTransaksiMLaundryResponse;
import com.mai.timor.model.json.book.detailTransaksi.GetDataTransaksiMMartResponse;
import com.mai.timor.model.json.book.detailTransaksi.GetDataTransaksiMSendResponse;
import com.mai.timor.model.json.book.detailTransaksi.GetDataTransaksiRequest;
import com.mai.timor.model.json.book.GetFoodRestoRequestJson;
import com.mai.timor.model.json.book.GetFoodRestoResponseJson;
import com.mai.timor.model.json.book.GetKendaraanAngkutResponseJson;
import com.mai.timor.model.json.book.GetNearBoxRequestJson;
import com.mai.timor.model.json.book.GetNearBoxResponseJson;
import com.mai.timor.model.json.book.GetNearRideCarRequestJson;
import com.mai.timor.model.json.book.GetNearRideCarResponseJson;
import com.mai.timor.model.json.book.GetNearServiceRequestJson;
import com.mai.timor.model.json.book.GetNearServiceResponseJson;
import com.mai.timor.model.json.book.MboxRequestJson;
import com.mai.timor.model.json.book.MboxResponseJson;
import com.mai.timor.model.json.book.MserviceRequestJson;
import com.mai.timor.model.json.book.MserviceResponseJson;
import com.mai.timor.model.json.book.RequestFoodRequestJson;
import com.mai.timor.model.json.book.RequestFoodResponseJson;
import com.mai.timor.model.json.book.RequestMartRequestJson;
import com.mai.timor.model.json.book.RequestMartResponseJson;
import com.mai.timor.model.json.book.RequestRideCarRequestJson;
import com.mai.timor.model.json.book.RequestRideCarResponseJson;
import com.mai.timor.model.json.book.RequestSendRequestJson;
import com.mai.timor.model.json.book.RequestSendResponseJson;
import com.mai.timor.model.json.book.SearchRestoranFoodRequest;
import com.mai.timor.model.json.book.SearchRestoranFoodResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import com.mai.timor.model.json.book.massage.DetailTransaksiRequest;
import com.mai.timor.model.json.book.massage.DetailTransaksiResponse;
import com.mai.timor.model.json.book.massage.GetLayananMassageResponseJson;
import com.mai.timor.model.json.book.massage.RequestMassageRequestJson;
import com.mai.timor.model.json.book.massage.RequestMassageResponseJson;

/**
 * Created by bradhawk on 10/17/2016.
 */

public interface BookService {

    @POST("book/list_driver_mride")
    Call<GetNearRideCarResponseJson> getNearRide(@Body GetNearRideCarRequestJson param);

    @POST("book/list_driver_mcar")
    Call<GetNearRideCarResponseJson> getNearCar(@Body GetNearRideCarRequestJson param);

    @POST("book/request_transaksi")
    Call<RequestRideCarResponseJson> requestTransaksi(@Body RequestRideCarRequestJson param);

    @POST("book/request_transaksi_mmart")
    Call<RequestMartResponseJson> requestTransaksiMMart(@Body RequestMartRequestJson param);

    @POST("book/request_transaksi_msend")
    Call<RequestSendResponseJson> requestTransMSend(@Body RequestSendRequestJson param);

    @GET("book/get_kendaraan_angkut")
    Call<GetKendaraanAngkutResponseJson> getKendaraanAngkut();

    @POST("book/list_driver_mbox")
    Call<GetNearBoxResponseJson> getNearBox(@Body GetNearBoxRequestJson param);

    @POST("book/request_transaksi_mbox")
    Call<MboxResponseJson> requestTransaksiMbox(@Body MboxRequestJson param);

    @GET("book/get_additional_mbox")
    Call<GetAdditionalMboxResponseJson> getAdditionalMbox();

    @POST("book/list_driver_mservice")
    Call<GetNearServiceResponseJson> getNearService(@Body GetNearServiceRequestJson param);

    @POST("book/request_transaksi_mservice")
    Call<MserviceResponseJson> requestTransaksi(@Body MserviceRequestJson param);

    @GET("book/get_data_mservice_ac")
    Call<GetDataMserviceResponseJson> getDataMservice();

    @GET("book/get_layanan_massage")
    Call<GetLayananMassageResponseJson> getLayananMassage();


    @POST("book/request_transaksi_mmassage")
    Call<RequestMassageResponseJson> requestTransaksiMMassage(@Body RequestMassageRequestJson param);

    @POST("book/list_driver_mmassage")
    Call<GetNearRideCarResponseJson> getNearMassage(@Body GetNearRideCarRequestJson param);

    @POST("book/get_data_transaksi_mmassage")
    Call<DetailTransaksiResponse> getDetailTransaksiMassage(@Body DetailTransaksiRequest param);

    @POST("book/get_data_restoran")
    Call<GetDataRestoResponseJson> getDataRestoran(@Body GetDataRestoRequestJson param);

    @POST("book/get_data_resto")
    Call<GetDataRestoranResponseJson> getDataRestoran1(@Body GetDataRestoranRequestJson param);

    @POST("book/get_food_in_resto")
    Call<GetFoodRestoResponseJson> getFoodResto(@Body GetFoodRestoRequestJson param);

    //Search item resto
    @POST("book/get_item_in_resto")
    Call<GetFoodRestoResponseJson> getMenuItem(@Body GetFoodRestoRequestJson param);

    @POST("book/search_restoran_or_food")
    Call<SearchRestoranFoodResponse> searchRestoranOrFood(@Body SearchRestoranFoodRequest param);

    @POST("book/get_resto_by_kategori")
    Call<GetDataRestoByKategoriResponseJson> getDataRestoranByKategori(@Body GetDataRestoByKategoriRequestJson param);

    @POST("book/request_transaksi_mfood")
    Call<RequestFoodResponseJson> requestTransaksiMFood(@Body RequestFoodRequestJson param);

    @POST("book/check_status_transaksi")
    Call<CheckStatusTransaksiResponse> checkStatusTransaksi(@Body CheckStatusTransaksiRequest param);

    @GET("book/liat_lokasi_driver/{id}")
    Call<LiatLokasiDriverResponse> liatLokasiDriver(@Path("id") String idDriver);

    @POST("book/get_data_order_mmassage")
    Call<String> getDataOrderMMassage(@Body GetDataTransaksiRequest param);

    @POST("book/get_data_transaksi_mfood")
    Call<String> getDataTransaksiMFood(@Body GetDataTransaksiRequest param);

    @POST("book/get_data_transaksi_mfood")
    Call<GetDataTransaksiMFoodResponse> getDataTransaksiMFoodDetail(@Body GetDataTransaksiRequest param);

    @POST("book/get_data_transaksi_mservice")
    Call<String> getDataTransaksiMService(@Body GetDataTransaksiRequest param);

    @POST("book/get_data_transaksi_mmart")
    Call<GetDataTransaksiMMartResponse> getDataTransaksiMMart(@Body GetDataTransaksiRequest param);

    @POST("book/get_data_transaksi_mbox")
    Call<String> getDataTransaksiMBox(@Body GetDataTransaksiRequest param);

    @POST("book/get_data_transaksi_msend")
    Call<GetDataTransaksiMSendResponse> getDataTransaksiMSend(@Body GetDataTransaksiRequest param);


    //service e-electronic
    @POST("book/get_item_in_store")
    Call<GetBarangTokoResponseJson> getBarangToko(@Body GetBarangTokoRequestJson param);
    @POST("book/get_data_store")
    Call<GetDataTokoResponseJson> getDataToko(@Body GetDataTokoRequestJson param);
    @POST("book/get_store_by_kategori")
    Call<GetDataTokoByKategoriResponseJson> getDataTokoByKategori(@Body GetDataTokoByKategoriRequestJson param);
    @POST("book/request_transaksi_mstore")
    Call<RequestBarangResponseJson> requestTransaksiEletronik(@Body RequestBarangRequestJson param);
    @POST("book/search_store_or_item")
    Call<SearchElectronicResponse> searchTokoOrElektronik(@Body SearchElectronicRequest param);
    @POST("book/get_data_transaksi_mstore")
    Call<GetDataTransaksiMElectronicResponse> getDataTransaksiMElectronicDetail(@Body GetDataTransaksiRequest param);

    //service e-laundry
    @POST("book/get_data_laundry")
    Call<GetDataLaundryResponseJson> getDataLaundry(@Body GetDataLaundryRequestJson param);


    @POST("book/get_promo_in_laundry") //SLIDE PROMO LAUNDRY.. GET DATA
    Call<GetLayananLaundryResponseJson> getPromoLaundry(@Body GetLayananLaundryRequestJson param);
    @POST("book/get_laundry_in_laundry")
    Call<GetLayananLaundryResponseJson> getSearchLaundry(@Body GetLayananLaundryRequestJson param);
    @POST("book/search_laundry")
    Call<SearchLaundryResponse> searchLaundry(@Body SearchLaundryRequest param);
    @POST("book/get_item_in_laundry")
    Call<GetLayananLaundryResponseJson> getMenuLaundry(@Body GetLayananLaundryRequestJson param);
    @POST("book/request_transaksi_mlaundry")
    Call<RequestLaundryResponseJson> requestTransaksiMLaundry(@Body RequestLaundryRequestJson param);
    @POST("book/get_data_transaksi_mlaundry")
    Call<GetDataTransaksiMLaundryResponse> getDataTransaksiMLaundryDetail(@Body GetDataTransaksiRequest param);


}
