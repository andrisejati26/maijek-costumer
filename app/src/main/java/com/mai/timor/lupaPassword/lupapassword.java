package com.mai.timor.lupaPassword;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mai.timor.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class lupapassword extends AppCompatActivity {

    @BindView(R.id.hubungi_kami_lupa)
    TextView textHubungiKamiLupa;

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupapassword);
        ButterKnife.bind(this);

        myDialog = new Dialog(this);

        textHubungiKamiLupa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imgWA, imgTel, imgSms;
                myDialog.setContentView(R.layout.hubungipopup);
                imgWA =(ImageView) myDialog.findViewById(R.id.waHUB);
                imgSms = (ImageView) myDialog.findViewById(R.id.smsHUB);
                imgTel =(ImageView) myDialog.findViewById(R.id.telHUB);
                imgWA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent waIntent =
                                new Intent("android.intent.action.VIEW",
                                        Uri.parse("https://api.whatsapp.com/send?phone=6289630022265"));
                        startActivity(waIntent);
                    }
                });
                imgSms.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri uri = Uri.parse("smsto:089630022265");
                        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                        it.putExtra("sms_body", "Butuh Bantuan!");
                        startActivity(it);
                    }
                });
                imgTel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String nomor = "089630022265" ;
                        Intent panggil = new Intent(Intent. ACTION_DIAL);
                        panggil.setData(Uri. fromParts("tel",nomor,null));
                        startActivity(panggil);
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();
            }
        });
    }
}
