package com.mai.timor.utils;

/**
 * Created by bradhawk on 10/29/2016.
 */

public interface MenuSelector {
    void selectMenu(int position);
}
