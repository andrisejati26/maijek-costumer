package com.mai.timor.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.mai.timor.model.FirebaseToken;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by bradhawk on 10/13/2016.
 */

public class MangJekInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        saveToken(FirebaseInstanceId.getInstance().getToken());
    }

    private void saveToken(String tokenId) {
        FirebaseToken token = new FirebaseToken(tokenId);
        EventBus.getDefault().postSticky(token);
    }

}
